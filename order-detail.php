<?php
    require_once "config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Order Details</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>
    <link rel="stylesheet" type="text/css" href="assets/css/order-detail.css?ver=<?php echo $randStr; ?>">    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="col-xs-12 col-sm-12 no-pad-lr main-wrapper">
        <div class="col-xs-12 col-sm-12 order-detail-main">
            <div class="col-xs-12 col-sm-12 no-pad-lr order-detail-main-inner">
              <div class="col-xs-12 col-sm-12 no-pad-lr">
                <div class="col-xs-12 col-sm-12 no-pad-lr order-detail-address-tab">
                    <h4>Address</h4>
                    <p>Kumarpatra pachali, Kumarpatra pachali,  Guwahati Assam India 110011</p>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 no-pad-lr">
                <div class="col-xs-12 col-sm-12 no-pad-lr order-details-tab">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Order Id</th>
                          <th>:</th>
                          <th>Order Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>#123456</td>
                          <td>:</td>
                          <td>Delivered</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
              </div>
              <div class="order-detail-table-list">
                <div class="col-xs-12 col-sm-12 no-pad-lr order-detail-table-main-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr order-detail-table-inner">
                      <div class="col-xs-12 col-sm-12 no-pad-lr order-detail-table-inner-head">
                          <div class="col-xs-5 order-detail-image-head col-sm-4">
                            <h4>Product Details</h4>
                          </div>
                          <div class="col-xs-2 col-sm-3">
                            <h4>Price</h4>
                          </div>
                          <div class="col-xs-3 col-sm-2">
                            <h4>Quantity</h4>
                          </div>
                          <div class="col-xs-2 col-sm-3 text-right">
                            <h4>Total Price</h4>
                          </div>
                        </div>
                      <div class="col-xs-12 col-sm-12 no-pad-lr order-detail-table-inner-item">
                          <div class="col-xs-5 col-sm-4 order-detail-image-title">
                            <div class="col-xs-4 col-sm-4 no-pad-lr order-detail-image">
                                <img src="assets/images/products/product3.jpeg" class="img-responsive">
                            </div>
                            <div class="col-xs-8 col-sm-8 order-detail-title">
                                <h3>Levis Jeans Fit</h3>
                            </div>
                          </div>
                          <div class="col-xs-2 col-sm-3 order-detail-price">
                            <p>$100.50</p>
                          </div>
                          <div class="col-xs-3 col-sm-2 order-detail-quantity">
                            <p>3</p>
                          </div>
                          <div class="col-xs-2 col-sm-3 order-detail-total text-right">
                            <p>$100.50</p>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 no-pad-lr order-detail-table-inner-item">
                          <div class="col-xs-5 col-sm-4 order-detail-image-title">
                            <div class="col-xs-4 col-sm-4 no-pad-lr order-detail-image">
                                <img src="assets/images/products/product3.jpeg" class="img-responsive">
                            </div>
                            <div class="col-xs-8 col-sm-8 order-detail-title">
                                <h3>Levis Jeans Fit</h3>
                            </div>
                          </div>
                          <div class="col-xs-2 col-sm-3 order-detail-price">
                            <p>$100.50</p>
                          </div>
                          <div class="col-xs-3 col-sm-2 order-detail-quantity">
                            <p>3</p>
                          </div>
                          <div class="col-xs-2 col-sm-3 order-detail-total text-right">
                            <p>$100.50</p>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 no-pad-lr order-detail-table-inner-item">
                          <div class="col-xs-5 col-sm-4 order-detail-image-title">
                            <div class="col-xs-4 col-sm-4 no-pad-lr order-detail-image">
                                <img src="assets/images/products/product3.jpeg" class="img-responsive">
                            </div>
                            <div class="col-xs-8 col-sm-8 order-detail-title">
                                <h3>Levis Jeans Fit</h3>
                            </div>
                          </div>
                          <div class="col-xs-2 col-sm-3 order-detail-price">
                            <p>$100.50</p>
                          </div>
                          <div class="col-xs-3 col-sm-2 order-detail-quantity">
                            <p>3</p>
                          </div>
                          <div class="col-xs-2 col-sm-3 order-detail-total text-right">
                            <p>$100.50</p>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
        </div>


        <?php include 'includes/footer.php'; ?>
    </div>

    <?php require_once 'includes/script.php'; ?>
  </body>
</html>