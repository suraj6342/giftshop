<?php
    require_once "config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Your Cart</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>

    <link rel="stylesheet" type="text/css" href="assets/css/cart.css?ver=<?php echo $randStr; ?>">
    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="col-xs-12 col-sm-12 no-pad-lr main-wrapper">
        <div class="col-xs-12 col-sm-12 cart-main">
            <div class="col-xs-12 col-sm-12 no-pad-lr cart-main-inner">
                <div class="col-xs-12 col-sm-12 cart-head">
                  <h2 class="pull-left">Your Cart</h2>
                  <h2 class="pull-right">3 Items</h2>
                </div>
                <div class="col-xs-12 col-sm-12 no-pad-lr cart-main-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr cart-main-tab-inner">
                      <div class="col-xs-12 col-sm-12 no-pad-lr cart-main-tab-inner-head">
                          <div class="col-xs-4 col-sm-4 tab-xs">
                            <h4>Product Details</h4>
                          </div>
                          <div class="col-xs-2 col-sm-2 tab-xs">
                            <h4>Price</h4>
                          </div>
                          <div class="col-xs-3 col-sm-2 tab-xs">
                            <h4>Quantity</h4>
                          </div>
                          <div class="col-xs-2 col-sm-2 tab-xs">
                            <h4>Total Price</h4>
                          </div>
                          <div class="col-xs-1"></div>
                        </div>
                      <div class="col-xs-12 col-sm-12 no-pad-lr cart-main-tab-inner-item">
                          <div class="col-xs-4 col-sm-4 cart-product-image-title">
                            <div class="col-xs-4 col-sm-4 no-pad-lr cart-product-image">
                                <img src="assets/images/products/product3.jpeg" class="img-responsive">
                            </div>
                            <div class="col-xs-8 col-sm-8 cart-product-title">
                                <h3>Levis Jeans Fit</h3>
                            </div>
                          </div>
                          <div class="col-xs-2 col-sm-2 cart-product-price">
                            <p>$100.50</p>
                          </div>
                          <div class="col-xs-3 col-sm-2 cart-product-quantity">
                              <div class="form-group quantity">
                                <input type='button' value='-' class='qtyminus minus' field='quantity' />
                                <input type='text' name='quantity' value='0' class='qty' />
                                <input type='button' value='+' class='qtyplus plus' field='quantity' />
                              </div>
                          </div>
                          <div class="col-xs-2 col-sm-2 cart-product-total">
                            <p>$100.50</p>
                          </div>
                          <div class="col-xs-1 col-sm-2 cart-product-delete">
                            <a href=""><img src="assets/images/icon/delete.svg" class="img-responsive"></a>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 no-pad-lr cart-main-tab-inner-item">
                          <div class="col-xs-4 col-sm-4 cart-product-image-title">
                            <div class="col-xs-4 col-sm-4 no-pad-lr cart-product-image">
                                <img src="assets/images/products/product2.jpeg" class="img-responsive">
                            </div>
                            <div class="col-xs-8 col-sm-8 cart-product-title">
                                <h3>Reebok Sport</h3>
                            </div>
                          </div>
                          <div class="col-xs-2 col-sm-2 cart-product-price">
                            <p>$100.50</p>
                          </div>
                          <div class="col-xs-3 col-sm-2 cart-product-quantity">
                              <div class="form-group quantity">
                                <input type='button' value='-' class='qtyminus minus' field='quantity' />
                                <input type='text' name='quantity' value='0' class='qty' />
                                <input type='button' value='+' class='qtyplus plus' field='quantity' />
                              </div>
                          </div>
                          <div class="col-xs-2 col-sm-2 cart-product-total">
                            <p>$100.50</p>
                          </div>
                          <div class="col-xs-1 col-sm-2 cart-product-delete">
                            <a href=""><img src="assets/images/icon/delete.svg" class="img-responsive"></a>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 no-pad-lr cart-main-tab-inner-item">
                          <div class="col-xs-4 col-sm-4 cart-product-image-title">
                            <div class="col-xs-4 col-sm-4 no-pad-lr cart-product-image">
                                <img src="assets/images/products/product6.jpeg" class="img-responsive">
                            </div>
                            <div class="col-xs-8 col-sm-8 cart-product-title">
                                <h3>Jeans Shirt</h3>
                            </div>
                          </div>
                          <div class="col-xs-2" col-sm-2 cart-product-price">
                            <p>$100.50</p>
                          </div>
                          <div class="col-xs-3 col-sm-2 cart-product-quantity">
                              <div class="form-group quantity">
                                <input type='button' value='-' class='qtyminus minus' field='quantity' />
                                <input type='text' name='quantity' value='0' class='qty' />
                                <input type='button' value='+' class='qtyplus plus' field='quantity' />
                              </div>
                          </div>
                          <div class="col-xs-2 col-sm-2 cart-product-total">
                            <p>$100.50</p>
                          </div>
                          <div class="col-xs-1 col-sm-2 cart-product-delete">
                            <a href=""><img src="assets/images/icon/delete.svg" class="img-responsive"></a>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 no-pad-lr shipping-tab">
                    <div class="col-xs-12 col-sm-7 no-pad-l address-tab">
                        <div class="col-xs-12 col-sm-12 no-pad-lr address-tab-inner">
                            <div class="col-xs-12 col-sm-12 address-tab-inner-head">
                                <h4>Shipping Details</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 address-tab-inner-details">
                                <table>
                                  <tbody>
                                    <tr>
                                      <th>First Name :</th>
                                      <td>Mr. Xyz</td>
                                      <th>Last Name :</th>
                                      <td>Rogers</td>
                                    </tr>
                                    <tr>
                                      <th>Address 1 :</th>
                                      <td>Pandu, Adabari Tiniali. Guwahati</td>
                                      <th>Address 2 :</th>
                                      <td>Pandu, Adabari Tiniali. Guwahati</td>
                                    </tr>
                                    <tr>
                                      <th>City :</th>
                                      <td>Guwahati</td>
                                      <th>State :</th>
                                      <td>Assam</td>
                                    </tr>
                                    <tr>
                                      <th>Country :</th>
                                      <td>India</td>
                                      <th>Zipcode :</th>
                                      <td>702445</td>
                                    </tr>
                                    <tr>
                                      <th>Phone :</th>
                                      <td>83254455551</td>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 no-pad-r cart-summary-tab">
                        <div class="col-xs-12 col-sm-12 no-pad-lr cart-summary-tab-inner">
                            <div class="col-xs-12 col-sm-12 cart-summary-tab-inner-head">
                                <h4>Cart Summary</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 cart-summary-tab-inner-details">
                                <table>
                                  <tbody>
                                      <tr>
                                          <th>Subtotal</th>
                                          <td>$1400</td>
                                      </tr>
                                      <tr>
                                          <th>Total</th>
                                          <td>$1400</td>
                                      </tr>
                                  </tbody>
                                </table>
                            </div>
                            <div class="col-xs-12 col-sm-12 credit-card-details">
                                <h4>Credit Card Details ( <p class="inner-mes">We don't store your card details</p> )</h4>
                                <input type="text" name="" class="credit-card-inputs" placeholder="Enter credit card number">
                                <div class="col-xs-12 col-sm-12 no-pad-lr">
                                    <div class="col-xs-4 col-sm-4 no-pad-l">
                                        <select class="credit-card-inputs">
                                            <option value="">Month</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>                                  
                                    </div>
                                    <div class="col-xs-4 col-sm-4">
                                        <select class="credit-card-inputs">
                                            <option value="">Year</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                            <option value="2029">2029</option>
                                        </select>                                  
                                    </div>
                                    <div class="col-xs-4 col-sm-4 no-pad-r">
                                      <input type="text" class="credit-card-inputs" placeholder="CVV" name="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 no-pad-lr place-order-btn">
                                <button class="main-btn">Place Order</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'includes/footer.php'; ?>
    </div>

    <?php require_once 'includes/script.php'; ?>
    <script type="text/javascript" src="assets/js/cart.js?ver=<?php echo $randStr; ?>"></script>
  </body>
</html>