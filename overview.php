<?php
    require_once "config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Your Account</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>    
    <link rel="stylesheet" type="text/css" href="assets/css/overview-wishlist-cart-common.css?ver=<?php echo $randStr; ?>">
    <link rel="stylesheet" type="text/css" href="assets/css/overview.css?ver=<?php echo $randStr; ?>">


  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="col-xs-12 col-sm-12 no-pad-lr main-wrapper">
        <div class="col-xs-12 col-sm-12 tabs-main">
            <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner">
              <div class="col-xs-4 col-sm-4 tabs-main-inner-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner-tab-inner">
                      <a href="overview.php" class="active">
                          <img src="assets/images/icon/overview.svg" class="img-responsive">
                          <h2>Overview</h2>
                          <p>A snapshot of your account information</p>
                      </a>
                  </div>
              </div>
              <div class="col-xs-4 col-sm-4 tabs-main-inner-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner-tab-inner">
                      <a href="order-history.php">
                          <img src="assets/images/icon/order-history.svg" class="img-responsive">
                          <h2>Order History</h2>
                          <p>See Current Orders and track shipments</p>
                      </a>
                  </div>
              </div>
              <div class="col-xs-4 col-sm-4 tabs-main-inner-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner-tab-inner">
                      <a href="wishlist.php">
                          <img src="assets/images/icon/wishlist.svg" class="img-responsive">
                          <h2>Wishlist</h2>
                          <p>Your Wishlist</p>
                      </a>
                  </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 account-login-tab">
              <div class="col-xs-12 col-sm-12 no-pad-lr account-login-tab-inner">
                <div class="col-xs-12 col-sm-12 text-center account-login-head">
                    <h4>Sign In</h4>
                    <hr class="head-line">
                </div>
                <div class="col-xs-12 col-sm-6 sign-in-tab">
                    <h4>Returning Customer</h4>
                    <hr class="head-line">
                    <div class="col-xs-12 col-sm-12 no-pad-lr sign-in-tab-inputs">
                        <input type="text" name="" placeholder="Email Address or Phone" class="main-input">
                        <input type="password" name="" placeholder="Password" class="main-input">
                    </div>
                    <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-6 col-sm-6 no-pad-lr remember-tab">
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Remember me
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 no-pad-lr forget-pass-tab">
                            <a href="">Forget Your Password ?</a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 no-pad-lr text-center login-btn-tab">
                        <button class="main-btn">Login</button>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 sign-up-tab">
                    <h4>Create An Account</h4>
                    <hr class="head-line">
                    <div class="col-xs-12 col-sm-12 sign-up-input-tab no-pad-lr">
                      <div class="col-xs-6 col-sm-6 no-pad-l">
                          <input type="text" name="" placeholder="First Name" class="main-input">
                      </div>
                      <div class="col-xs-6 col-sm-6 no-pad-r">
                          <input type="password" name="" placeholder="Last Name" class="main-input">
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <input type="text" name="" placeholder="Email Address" class="main-input">
                    </div>
                    <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <input type="text" name="" placeholder="Phone Number" class="main-input">
                    </div>
                    <div class="col-xs-12 col-sm-12 no-pad-lr">
                      <div class="col-xs-6 col-sm-6 no-pad-l">
                          <input type="password" name="" placeholder="Password" class="main-input">
                      </div>
                      <div class="col-xs-6 col-sm-6 no-pad-r">
                          <input type="password" name="" placeholder="Confirm Password" class="main-input">
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 no-pad-lr text-center create-account-btn">
                      <button class="main-btn">Create Account</button>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 after-login-tab">
              <div class="col-xs-12 col-sm-6 no-pad-l">
                <div class="col-xs-12 col-sm-12 after-login-left-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr after-login-tab-left-head">
                      <h4 class="pull-left">Sign In Information</h4>
                      <button class="edit-btn pull-right" data-toggle="modal" data-target="#update-detail-modal">Edit</button>
                  </div>
                  <div class="col-xs-12 col-sm-12 no-pad-lr after-login-info">
                      <ul>
                            <li>Suraj Singh Rawat</li>
                            <li>suraj63421@gmail.com <a href="">( Varify Email )</a></li>
                            <li>8375986464</li>
                            <li>Male</li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 no-pad-r">
                <div class="col-xs-12 col-sm-12 after-login-right-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr after-login-tab-right-head">
                      <h4 class="pull-left">Billing Address</h4>
                      <button class="edit-btn pull-right" data-toggle="modal" data-target="#update-address-modal">Edit</button>
                  </div>
                  <div class="col-xs-12 col-sm-12 no-pad-lr after-login-info">
                      <ul>
                            <li>Suraj Rawat</li>
                            <li>Kumarpatra pachali</li>
                            <li>Kumarpatra pachali</li>
                            <li>Guwahati Assam India 110011</li>
                            <li>8385986464</li>
                      </ul>
                  </div>
                </div>
              </div>
            </div>
        </div>

        <!-- update profile Modal -->
        <div class="modal fade" id="update-detail-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Profile</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 no-pad-lr">
                    <div class="col-xs-12 col-sm-6">
                      <input type="text" name="" class="main-input" placeholder="First Name">
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <input type="text" name="" class="main-input" placeholder="Last Name">
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12">
                    <input type="text" name="" class="main-input" placeholder="Email Address">
                  </div>
                  <div class="col-xs-12 col-sm-12 no-pad-lr">
                    <div class="col-xs-12 col-sm-6">
                      <select class="main-input"> 
                        <option value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                      </select>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <input type="text" name="" class="main-input" placeholder="Phone Number">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button class="main-btn">Update</button>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="update-address-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Billing Address</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 no-pad-lr">
                    <div class="col-xs-6 col-sm-6">
                      <input type="text" name="" class="main-input" placeholder="First Name">
                    </div>
                    <div class="col-xs-6 col-sm-6">
                      <input type="text" name="" class="main-input" placeholder="Last Name">
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 no-pad-lr">
                    <div class="col-xs-12 col-sm-6">
                      <textarea class="main-input" rows="2" placeholder="Address Line 1"></textarea>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <textarea class="main-input" rows="2" placeholder="Address Line 2"></textarea>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 no-pad-lr">
                    <div class="col-xs-6 col-sm-6">
                      <input type="text" name="" class="main-input" placeholder="Phone Number">
                    </div>
                    <div class="col-xs-6 col-sm-6">
                      <input type="text" name="" class="main-input" placeholder="Country">
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 no-pad-lr">
                    <div class="col-xs-4 col-sm-4">
                      <input type="text" name="" class="main-input" placeholder="State">
                    </div>
                    <div class="col-xs-4 col-sm-4">
                      <input type="text" name="" class="main-input" placeholder="City">
                    </div>
                    <div class="col-xs-4 col-sm-4">
                      <input type="text" name="" class="main-input" placeholder="Zip">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button class="main-btn">Submit</button>
              </div>
            </div>
          </div>
        </div>

        <?php include 'includes/footer.php'; ?>
    </div>

    <?php require_once 'includes/script.php'; ?>
  </body>
</html>