<div class="navbar">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-sm-12 nav-inner">
          <div class="col-xs-12 col-sm-4 search-bar">
            <form class="col-xs-12 col-sm-12 no-pad-lr">
              <div>
                  <input type="text" name="" placeholder="Search">
                  <button class="btn search-submit">
                      <img src="assets/images/icon/search.svg">
                  </button>
              </div>
            </form>
          </div>
          <div class="col-xs-12 col-sm-4 brand-logo">
            <a href="index.php"><img src="assets/images/logo2.png" class="img-responsive"></a>
            <div class="hidden-sm hidden-md hidden-lg mobile-nav-menu">
              <div class="menu-wrapper">
                <div class="hamburger-menu"></div>    
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 text-right navbar-right-tab hidden-xs">
               <a href="overview.php" class="user-account" title="Account"><i class="far fa-user"></i></a>
               <a href="wishlist.php" title="Wishlist" class="wishlist"><i class="far fa-heart"></i></a>
               <a href="cart.php" title="Cart" class="cart"><i class="fas fa-shopping-basket"></i><span class="badge">0</span></a>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 main-menu hidden-xs">
          <!-- <div class="col-xs-3 col-sm-3 no-pad-lr categories-menu-hover">
            <a href="" class="categories-menu-tab">Categories <i class="fas fa-angle-down"></i></a>
            <div class="col-xs-12 col-sm-12 no-pad-lr category-menu-wrapper">
              <ul class="category-menu-tab">
                <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Mens</span> 
                      <span class="badge">NEW</span>
                    </a>
                </li>
                <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Womens</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                </li>
                <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Accessories</span>
                      <span class="badge">NEW</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                </li>
                <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Bags</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                </li>
                <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Watches</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                </li>
                <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Home Decore</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                </li>
              </ul>
            </div>
          </div> -->
          <div class="col-xs-12 col-sm-12 text-center main-menu-sm">
            <ul class="list-inline main-menu-tabs">
              <li class="menu-parent">
                <a href="products.php" class="menu">Mens <i class="fas fa-angle-down"></i>
                </a>
                <ul class="sub-menu">
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Shirt</span>

                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Pants</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Jeans</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Socks</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Hat</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Watch</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                </ul>
              </li>
              <li class="menu-parent">
                <a href="products.php" class="menu">Womens <i class="fas fa-angle-down"></i></a>
                <ul class="sub-menu">
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Shirt</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Pants</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Jeans</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Socks</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Hat</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Watch</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                </ul>
              </li>
              <li class="menu-parent">
                <a href="products.php" class="menu">Accessories <i class="fas fa-angle-down"></i></a>
                <ul class="sub-menu">
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Shirt</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Pants</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Jeans</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Socks</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Hat</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                  <li>
                    <a href="products.php" class="main-menu-menu-tab">
                      <i class="fas fa-angle-right prev"></i>
                      <span>Watch</span>
                      <i class="fas fa-angle-right next"></i>
                    </a> 
                  </li>
                </ul>
              </li>
              <li class="menu-parent">
                <a href="" class="menu">About Us <i class="fas fa-angle-down"></i></a>
              </li>
              <li>
                <a href="contact-us.php" class="menu">Contact <i class="fas fa-angle-down"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
</div>