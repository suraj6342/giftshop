<?php
    require_once "config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Web Design</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>
    
    <link rel="stylesheet" type="text/css" href="assets/css/index.css?ver=<?php echo $randStr; ?>">    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="main-wrapper">
      <div class="col-xs-12 col-sm-12 no-pad-lr main-slider">
        <div class="slider__warpper">
            <div class="flex__container flex--pikachu flex--active" data-slide="1">
              <div class="flex__item flex__item--left">
                <div class="flex__content">
                  <h1 class="text--big">Lorem ipsum dolor sit amet</h1>
                  <p class="text--normal">Pikachu is an Electric-type Pokémon introduced in Generation I. Pikachu are small, chubby, and incredibly cute mouse-like Pokémon. They are almost completely covered by yellow fur.</p>
                </div>
              </div>
              <div class="flex__item flex__item--right"></div>
              <img class="model-img" src="assets/images/slider1.png" />
            </div>
            <div class="flex__container flex--piplup animate--start" data-slide="2">
              <div class="flex__item flex__item--left">
                <div class="flex__content">
                  <h1 class="text--big">Bonorum singulis hendrerit ei has</h1>
                  <p class="text--normal">Piplup is the Water-type Starter Pokémon of the Sinnoh region. It was introduced in Generation IV. Piplup has a strong sense of self-esteem. It seldom accepts food that people give because of its pride.</p>
                </div>
              </div>
              <div class="flex__item flex__item--right"></div>
              <img class="model-img" src="assets/images/slider2.png" />
            </div>
            <div class="flex__container flex--blaziken animate--start" data-slide="3">
              <div class="flex__item flex__item--left">
                <div class="flex__content">
                  <h1 class="text--big">Ne vis scripta interesset</h1>
                  <p class="text--normal">Blaziken is the Fire/Fighting-type Starter Pokémon of the Hoenn region, introduced in Generation III. Blaziken is a large, bipedal, humanoid bird-like Pokémon that resembles a rooster.</p>
                </div>
              </div>
              <div class="flex__item flex__item--right"></div>
              <img class="model-img" src="assets/images/slider3.png" />
            </div>
            <div class="flex__container flex--dialga animate--start" data-slide="4">
              <div class="flex__item flex__item--left">
                <div class="flex__content">
                  <h1 class="text--big">Propriae gloriatur eam cu</h1>
                  <p class="text--normal">Dialga is a Steel/Dragon-type Legendary Pokémon. Dialga is a sauropod-like Pokémon. It is mainly blue with some gray, metallic portions, such as its chest plate, which has a diamond in the center. It also has various, light blue lines all over
                    its body.</p>
                </div>
              </div>
              <div class="flex__item flex__item--right"></div>
              <img class="model-img" src="assets/images/slider4.png" />
            </div>
          </div>

          <div class="slider__navi">
            <a href="#" class="slide-nav active" data-slide="1">pikachu</a>
            <a href="#" class="slide-nav" data-slide="2">piplup</a>
            <a href="#" class="slide-nav" data-slide="3">blaziken</a>
            <a href="#" class="slide-nav" data-slide="4">dialga</a>
          </div>
      </div>
      <div class="col-xs-12 col-sm-12 no-pad-lr category-slider-main">
          <div class="col-xs-12 col-sm-3 no-pad-lr category-slider-head">
            <h2>Our Category</h2>
            <p>Lorem ipsum dolor sit amet nui tew</p>
          </div>
          <div class="col-xs-12 col-sm-9 no-pad-lr category-slider">
              <div class="jcarousel-wrapper">
                  <div class="jcarousel" id="category-carousel">
                      <ul>
                        <li>
                          <div class="category-slider-tabs">
                            <a href="#">
                              <img src="assets/images/icon/shoe.svg" class="img-responsive">
                              <p>Shoes</p>
                            </a>
                          </div>
                        </li>
                        <li>
                          <div class="category-slider-tabs">
                            <a href="#">
                              <img src="assets/images/icon/dress.svg" class="img-responsive">
                              <p>Shoes</p>
                            </a>
                          </div>
                        </li>
                        <li>
                          <div class="category-slider-tabs">
                            <a href="#">
                              <img src="assets/images/icon/glasses.svg" class="img-responsive">
                              <p>Shoes</p>
                            </a>
                          </div>
                        </li>
                        <li>
                          <div class="category-slider-tabs">
                            <a href="#">
                              <img src="assets/images/icon/jeans.svg" class="img-responsive">
                              <p>Shoes</p>
                            </a>
                          </div>
                        </li>
                        <li>
                          <div class="category-slider-tabs">
                              <a href="#">
                                <img src="assets/images/icon/bag.svg" class="img-responsive">
                                <p>Shoes</p>
                              </a>
                          </div>
                        </li>
                        <li>
                          <div class="category-slider-tabs">
                              <a href="">
                                <img src="assets/images/icon/smartwatch.svg" class="img-responsive">
                                <p>Shoes</p>
                              </a>
                          </div>
                        </li>
                        <li>
                          <div class="category-slider-tabs">
                            <a href="">
                              <img src="assets/images/icon/t-shirt.svg" class="img-responsive">
                              <p>Shoes</p>
                            </a>
                          </div>
                        </li>
                        <li>
                          <div class="category-slider-tabs">
                            <a href="">
                              <img src="assets/images/icon/scarf.svg" class="img-responsive">
                              <p>Shoes</p>
                            </a>  
                          </div>
                        </li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-xs-12 col-sm-12 no-pad-lr discount-offer-banners">
        <div class="col-xs-12 col-sm-6 no-pad-lr discount-offer-banners-left">
            <div class="col-xs-12 col-sm-12 discount-banners-left-inner">
                <div class="col-xs-12 col-sm-12 no-pad-lr banner-left-left">
                  <div class="col-xs-12 col-sm-12 no-pad-lr banner-left-left-inner">
                    <h3>Women Fashion</h3>
                    <p>Lorem ipsum dolor sit amet ex dolorum.</p>
                    <div class="col-xs-12 col-sm-12 no-pad-lr discount-number">
                      <p><sup>Upto </sup>70% <sub>Off</sub></p>
                      <div class="col-xs-12 col-sm-12 no-pad-lr lines-hr">
                        <span></span>
                        <span></span>
                        <span></span>
                      </div>
                    </div>
                    <a href="products.php" class="main-btn">Shop Now</a>
                  </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 no-pad-lr discount-offer-banners-right">
            <div class="col-xs-12 col-sm-12 discount-banners-right-inner">
                <div class="col-xs-12 col-sm-12 no-pad-lr banner-right-top">
                  <div class="col-xs-12 col-sm-12 no-pad-lr banner-right-top-inner">
                    <h3>Men Fashion</h3>
                    <p>Lorem ipsum dolor sit ex dolorum.</p>
                    <div class="col-xs-12 col-sm-12 no-pad-lr discount-number">
                      <p><sup>Upto </sup>70% <sub>Off</sub></p>
                      <div class="col-xs-12 col-sm-12 no-pad-lr lines-hr mobile-line-hr">
                        <span></span>
                        <span></span>
                        <span></span>
                      </div>
                    </div>
                    <a href="products.php" class="main-btn">Shop Now</a>
                  </div>
                </div>
            </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 no-pad-lr product-tabs-main">
        <div class="col-xs-12 col-sm-12 product-tabs-main-head text-center">
          <h2>Featured Collections</h2>
          <p>vestibulum feugiat quam et sem bibendum ac</p>
        </div>
        <div class="col-xs-12 col-sm-12 product-tabs-main-tab">
          <div>
            <div class="col-xs-12 col-sm-12 no-pad-lr">
              <div class="jcarousel-wrapper slider-wrapper">
                <div class="jcarousel" id="featured-slider">
                  <ul>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product1.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product2.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product3.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product4.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product5.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product6.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product7.jpg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product8.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <a href="#" class="jcarousel-control-prev " id="prev-one">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next " id="next-one">&rsaquo;</a>
                <a href="" class="pull-right more-btn">more</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 no-pad-lr product-tabs-main">
        <div class="col-xs-12 col-sm-12 product-tabs-main-head text-center">
          <h2>New Collections</h2>
          <p>vestibulum feugiat quam et sem bibendum ac</p>
        </div>
        <div class="col-xs-12 col-sm-12 product-tabs-main-tab">
          <div>
            <div class="col-xs-12 col-sm-12 no-pad-lr">
              <div class="jcarousel-wrapper slider-wrapper">
                <div class="jcarousel" id="newcollection">
                  <ul>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product1.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product2.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product3.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product4.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product5.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product6.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product7.jpg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product8.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4>Red Top</h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">$566</span>
                                </div>
                            </a>
                        </div>
                      </div>
                    </li>

                  </ul>
                </div>
                <a href="#" class="jcarousel-control-prev " id="prev-one">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next " id="next-one">&rsaquo;</a>
                <a href="" class="pull-right more-btn">more</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 no-pad-lr service-feature">
        <div class="col-xs-6 col-sm-3 service-feature-inner">
          <div class="col-xs-12 col-sm-12 service-feature-inner-tab">
            <img src="assets/images/icon/support.svg" class="img-responsive">
            <div class="col-xs-12 col-sm-12 no-pad-lr service-feature-inner-tab-content">
              <h4>Support 24/7</h4>
              <p>Lorem ipsum lore tue nue.</p>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-3 service-feature-inner">
          <div class="col-xs-12 col-sm-12 service-feature-inner-tab">
            <img src="assets/images/icon/shipping.svg" class="img-responsive">
            <div class="col-xs-12 col-sm-12 no-pad-lr service-feature-inner-tab-content">
              <h4>Free Shipping</h4>
              <p>Lorem ipsum lore tue nue.</p>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-3 service-feature-inner">
          <div class="col-xs-12 col-sm-12 service-feature-inner-tab">
            <img src="assets/images/icon/return.svg" class="img-responsive">
            <div class="col-xs-12 col-sm-12 no-pad-lr service-feature-inner-tab-content">
              <h4>Assured Returns</h4>
              <p>Lorem ipsum lore tue nue.</p>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-3 service-feature-inner">
          <div class="col-xs-12 col-sm-12 service-feature-inner-tab">
            <img src="assets/images/icon/security.svg" class="img-responsive">
            <div class="col-xs-12 col-sm-12 no-pad-lr service-feature-inner-tab-content">
              <h4>Secure Payment</h4>
              <p>Lorem ipsum lore tue nue.</p>
            </div>
          </div>
        </div>
      </div>
      <?php include 'includes/footer.php'; ?>
    </div>

    <?php require_once 'includes/script.php'; ?>
    <script type="text/javascript" src="assets/js/index.js?ver=<?php echo $randStr; ?>"></script>
  </body>
</html>