<?php
    require_once "config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Product Description</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>

    <link rel="stylesheet" type="text/css" href="assets/css/product-desc.css?ver=<?php echo $randStr; ?>">
    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="main-wrapper">
      
      <div class="col-xs-12 col-sm-12 no-pad-lr product-description-main"> 
        <div class="col-xs-12 col-sm-7 no-pad-lr product-description-left">
            <div class="col-xs-12 col-sm-12 no-pad-lr">
                <div id="product-desc-slider" class="carousel slide" data-ride="carousel" data-interval="false">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#product-desc-slider" data-slide-to="0" class="active">
                          <div class="thumbnail">
                              <img src="assets/images/products/product1.jpeg" alt="image1">
                          </div>
                        </li>
                        <li data-target="#product-desc-slider" data-slide-to="1">
                          <div class="thumbnail">
                              <img src="assets/images/products/product2.jpeg" alt="image1">
                          </div>
                        </li>
                        <li data-target="#product-desc-slider" data-slide-to="2">
                          <div class="thumbnail">
                              <img src="assets/images/products/product3.jpeg" alt="image1">
                          </div>
                        </li>
                        <li data-target="#product-desc-slider" data-slide-to="3">
                          <div class="thumbnail">
                              <img src="assets/images/products/product4.jpeg" alt="image1">
                          </div>
                        </li>
                        <li data-target="#product-desc-slider" data-slide-to="4">
                          <div class="thumbnail">
                              <img src="assets/images/products/product5.jpeg" alt="image1">
                          </div>
                        </li>
                        <li data-target="#product-desc-slider" data-slide-to="5">
                          <div class="thumbnail">
                              <img src="assets/images/products/product6.jpeg" alt="image1">
                          </div>
                        </li>
                        <li data-target="#product-desc-slider" data-slide-to="6">
                          <div class="thumbnail">
                              <img src="assets/images/products/product7.jpg" alt="image1">
                          </div>
                        </li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                <!--<button class="thumbnail-arrow arrow-up" onclick="scrollup()"><i class="fas fa-angle-up"></i></button>
                    <button class="thumbnail-arrow arrow-down" onclick="scrolldown()"><i class="fas fa-angle-down"></i></button>-->                        
                        <div class="item active">
                          <img src="assets/images/products/product1.jpeg" alt="image1">
                        </div>
                        <div class="item">
                          <img src="assets/images/products/product2.jpeg" alt="image1">
                        </div>
                        <div class="item">
                          <img src="assets/images/products/product3.jpeg" alt="image1">
                        </div>
                        <div class="item">
                          <img src="assets/images/products/product4.jpeg" alt="image1">
                        </div>
                        <div class="item">
                          <img src="assets/images/products/product5.jpeg" alt="image1">
                        </div>
                        <div class="item">
                          <img src="assets/images/products/product6.jpeg" alt="image1">
                        </div>
                        <div class="item">
                          <img src="assets/images/products/product7.jpg" alt="image1">
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#product-desc-slider" role="button" data-slide="prev">
                      <span class="fas fa-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#product-desc-slider" role="button" data-slide="next">
                      <span class="fas fa-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5 no-pad-lr product-description-right">
            <div class="col-xs-12 col-sm-12 no-pad-lr">
              <h2 class="product_title">Levis Jeans Black</h2>
              <p class="product_price">$1000</p>
              <p class="product_desc">Lorem ipsum dolor sit amet, nec diam numquam ex, ad qui prodesset temporibus theophrastus. Ex erant omnesque oportere has. At essent percipit est, sea verterem reprimique no.</p>
              <div class="color_pick">
                  <ul>
                    <li><p>Color : </p></li>
                    <li><a href="#" class="active">Yellow</a></li>
                    <li><a href="#">Black</a></li>
                    <li><a href="#">Blue</a></li>
                    <li><a href="#">Green</a></li>
                  </ul>
              </div>
              <div class="size_pick">
                  <ul>
                    <li><p>Size : </p></li>
                    <li><a href="#" class="active">S</a></li>
                    <li><a href="#">M</a></li>
                    <li><a href="#">L</a></li>
                    <li><a href="#">XL</a></li>
                  </ul>
              </div>
              <div class="add-cart-btn">
                <button class="btn main-btn">Add To Cart</button>
              </div>
              <div class="col-xs-12 col-sm-12 no-pad-lr availability-tab">
                  <div class="col-xs-3 col-sm-1 text-left">
                    <p>Availability</p>
                  </div>
                  <div class="col-xs-3 col-sm-1 text-center">
                    <p>:</p>
                  </div>
                  <div class="col-xs-3 col-sm-1 text-right">
                    <p>In Stock</p>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-12 no-pad-lr sku-tab">
                  <div class="col-xs-3 col-sm-1 text-left">
                    <p>SKU</p>
                  </div>
                  <div class="col-xs-3 col-sm-1 text-center">
                    <p>:</p>
                  </div>
                  <div class="col-xs-3 col-sm-1 text-right">
                    <p>USSH5571</p>
                  </div>
              </div>
            </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 no-pad-lr product-description-para">
        <div class="col-xs-12 col-sm-12 no-pad-lr product-description-inner">
          <h2>Description</h2>
          <p>Lorem ipsum dolor sit amet, semper nominavi offendit no pri, quo cu mutat mollis incorrupte. Ut suas eros alienum vix. Legendos definitionem his ut. Saepe honestatis vix no, purto liber et vim, at nec phaedrum volutpat. Agam modus ex eam. No neglegentur comprehensam pro, alia semper salutatus cu mel.</p>
          <p>Habemus dolores percipitur ex sea, homero aeterno honestatis an usu, pri ei homero intellegat. Periculis vulputate sed ut, virtute detraxit eos ne. Ea suas malorum deserunt vim. Odio sententiae at pri. Ex dolorum pertinax instructior mei, ut sit nihil eirmod posidonium.</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 no-pad-lr related-product-tabs-main">
        <div class="col-xs-12 col-sm-12 product-tabs-main-head text-left">
          <h2>Related Product</h2>
        </div>
        <div class="col-xs-12 col-sm-12 no-pad-lr product-tabs-main-tab">
          <div>
            <div class="col-xs-12 col-sm-12 no-pad-lr">
              <div class="jcarousel-wrapper slider-wrapper">
                <div class="jcarousel" id="newcollection">
                  <ul>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                  <img src="assets/images/products/product3.jpeg">                          
                              </div>
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                  <h4>Red Top</h4>
                                  <span class="list-price pull-left">Polo Shirts</span>
                                  <span class="price pull-right">$566</span>
                              </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                  <img src="assets/images/products/product2.jpeg">                          
                              </div>
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                  <h4>Red Top</h4>
                                  <span class="list-price pull-left">Polo Shirts</span>
                                  <span class="price pull-right">$566</span>
                              </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                  <img src="assets/images/products/product1.jpeg">                          
                              </div>
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                  <h4>Red Top</h4>
                                  <span class="list-price pull-left">Polo Shirts</span>
                                  <span class="price pull-right">$566</span>
                              </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                  <img src="assets/images/products/product4.jpeg">                          
                              </div>
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                  <h4>Red Top</h4>
                                  <span class="list-price pull-left">Polo Shirts</span>
                                  <span class="price pull-right">$566</span>
                              </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                  <img src="assets/images/products/product5.jpeg">                          
                              </div>
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                  <h4>Red Top</h4>
                                  <span class="list-price pull-left">Polo Shirts</span>
                                  <span class="price pull-right">$566</span>
                              </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                  <img src="assets/images/products/product6.jpeg">                          
                              </div>
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                  <h4>Red Top</h4>
                                  <span class="list-price pull-left">Polo Shirts</span>
                                  <span class="price pull-right">$566</span>
                              </div>
                            </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="col-xs-12 col-sm-12 no-pad-lr">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php">
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                  <img src="assets/images/products/product7.jpg">                          
                              </div>
                              <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                  <h4>Red Top</h4>
                                  <span class="list-price pull-left">Polo Shirts</span>
                                  <span class="price pull-right">$566</span>
                              </div>
                            </a>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <a href="#" class="jcarousel-control-prev " id="prev-one">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next " id="next-one">&rsaquo;</a>
                <a href="" class="pull-right more-btn">more</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <?php include 'includes/footer.php'; ?>
    </div>

    <?php require_once 'includes/script.php'; ?>
    <script type="text/javascript" src="assets/js/product-desc.js?ver=<?php echo $randStr; ?>"></script>
  </body>
</html>