<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>
        <link rel="stylesheet" type="text/css" href="css/view-products.css?ver=<?php echo $randStr; ?>">


  </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content">
    <?php include 'includes/navbar.php'; ?>
      <div class="container-fluid">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Add Varient</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form>
                <div class="">
                  <div class="row">
                    <div class="col-lg-11">
                      <div class="form-group">
                        <label class="form-control-label" for="product-title">Size *</label>
                        <input type="text" id="product-title" class="form-control form-control-alternative" placeholder="Username" value="lucky.jesse">
                      </div>
                    </div>
                    <div class="col-lg-11">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Primary Color *</label>
                        <textarea rows="4" class="form-control form-control-alternative" id="product-desc" >Description of the Prooduct</textarea>
                      </div>
                    </div>
                    <div class="col-lg-11">
                      <div class="form-group">
                        <label class="form-control-label" for="menu-item">Secondary Colors *</label>
                        <select type="text" id="menu-item" class="form-control form-control-alternative">
                          <option>Select Menu Item</option>
                          <option>Equipment</option>
                          <option>Machine</option>
                          <option>Laptop</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-11">
                      <div class="form-group">
                        <label class="form-control-label" for="menu-item">SKU *</label>
                        <select type="text" id="menu-item" class="form-control form-control-alternative">
                          <option>Select Category</option>
                          <option>Equipment</option>
                          <option>Machine</option>
                          <option>Laptop</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-11">
                      <div class="form-group">
                        <label class="form-control-label" for="menu-item">Stock Quantity *</label>
                        <select type="text" id="menu-item" class="form-control form-control-alternative">
                          <option>Select Sub Category</option>
                          <option>Equipment</option>
                          <option>Machine</option>
                          <option>Laptop</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-11">
                      <div class="form-group">
                        <label class="form-control-label" for="product-title">My Price *</label>
                        <input type="text" id="product-title" class="form-control form-control-alternative" placeholder="Username" value="lucky.jesse">
                      </div>
                    </div>
                    <div class="col-lg-11">
                      <div class="form-group">
                        <label class="form-control-label" for="product-title">MSRP *</label>
                        <input type="text" id="product-title" class="form-control form-control-alternative" value="">
                      </div>
                    </div>
                    <div class="col-lg-11">
                      <div class="form-group">
                        <label class="form-control-label" for="product-title">Sale Price: *</label>
                        <input type="text" id="product-title" class="form-control form-control-alternative" placeholder="Username" value="lucky.jesse">
                      </div>
                    </div>
                    <div class="col-lg-11">
                      <button class="btn btn-icon btn-3 btn-primary" type="button">
                          <span class="btn-inner--text">Submit</span>
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>    
    </div>
    <?php include 'includes/script.php'; ?>
  </body>
</html>