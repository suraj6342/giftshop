 $('#edit-page-btn').click(function(){
    $('.edit-section').show();
    tinymce.init({
        selector: '#page_content',
        height: 500,
        plugins: 'advlist autolink link image lists charmap print preview',
        toolbar1: 'newdocument, bold, italic, underline, strikethrough, alignleft, aligncenter, alignright, alignjustify, styleselect, formatselect, fontselect, fontsizeselect',
        toolbar2: 'cut, copy, paste, bullist, numlist, outdent, indent, blockquote, undo, redo, removeformat, subscript, superscript',
        images_upload_url: '../api/admin/pages/upload-images-tinymce.php',
        images_upload_base_path: '',
      });
    $('.view-section').hide();
});