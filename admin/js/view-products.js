function show_hide_variants(e){
	var parent = e.getAttribute('data-product-id');
	$('.variant-table-row').hide();
	$('#variant_'+parent).show();
}

// product image upload

$('.upload-form input').change(function () {
  $('.upload-form p').text(this.files.length + " file(s) selected");
});
