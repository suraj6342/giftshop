<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>
  </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content">
    <?php include 'includes/navbar.php'; ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card shadow">
              <div class="card-header border-0">
                <h3 class="mb-0 text-uppercase">Home page Content</h3>
              </div>
              <div class="table-responsive view-products-table">
                <table class="table align-items-center table-flush table-hover view-product-table">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Type</th>
                      <th scope="col">Link</th>
                      <th scope="col">Description</th>
                      <th scope="col">Create Date</th>
                      <th scope="col">Image</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td scope="row">
                          <span class="mb-0 text-sm">Slider</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">http://dosfeeds.com/ecommerce</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Ecommerce home</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">2019-02-12 10:57:47</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><a href="">View</a></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><button class="btn btn-danger">Delete</button></span>
                      </td>
                    </tr>
                    <tr>
                      <td scope="row">
                          <span class="mb-0 text-sm">Slider</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">http://dosfeeds.com/ecommerce</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Ecommerce home</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">2019-02-12 10:57:47</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><a href="">View</a></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><button class="btn btn-danger">Delete</button></span>
                      </td>
                    </tr>
                    <tr>
                      <td scope="row">
                          <span class="mb-0 text-sm">Slider</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">http://dosfeeds.com/ecommerce</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Ecommerce home</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">2019-02-12 10:57:47</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><a href="">View</a></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><button class="btn btn-danger">Delete</button></span>
                      </td>
                    </tr>
                    <tr>
                      <td scope="row">
                          <span class="mb-0 text-sm">Slider</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">http://dosfeeds.com/ecommerce</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Ecommerce home</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">2019-02-12 10:57:47</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><a href="">View</a></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><button class="btn btn-danger">Delete</button></span>
                      </td>
                    </tr>
                    <tr>
                      <td scope="row">
                          <span class="mb-0 text-sm">Slider</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">http://dosfeeds.com/ecommerce</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Ecommerce home</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">2019-02-12 10:57:47</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><a href="">View</a></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><button class="btn btn-danger">Delete</button></span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="card-footer py-4">
                <nav aria-label="...">
                  <ul class="pagination justify-content-end mb-0">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1">
                        <i class="fas fa-angle-left"></i>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item active">
                      <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#">
                        <i class="fas fa-angle-right"></i>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>    
      
      <!-- Update Product Status modal -->
      <div class="modal fade" id="update-product-status" tabindex="-1" role="dialog" aria-labelledby="update-product-statusLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="update-product-statusLabel">Update Product Status</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="form-control-label" for="menu-item">Product Status *</label>
                  <select type="text" id="menu-item" class="form-control form-control-alternative">
                    <option>Select Status</option>
                    <option>Active</option>
                    <option>Inactive</option>
                  </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Update</button>
            </div>
          </div>
        </div>
      </div>

      <!-- Update varient quality modal -->
      <div class="modal fade" id="update-variant-quantity" tabindex="-1" role="dialog" aria-labelledby="update-variant-quantityLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="update-variant-quantityLabel">Update Variant Quantity</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="form-control-label" for="menu-item">Stock Quantity *</label>
                  <input type="text" class="form-control form-control-alternative" placeholder="Stock Quality">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Update</button>
            </div>
          </div>
        </div>
      </div>


      <!-- Update varient status modal -->
      <div class="modal fade" id="update-variant-status" tabindex="-1" role="dialog" aria-labelledby="update-variant-statusLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="update-variant-statusLabel">Update Variant Status</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="form-control-label" for="menu-item">Product Vaient Status *</label>
                  <select type="text" id="menu-item" class="form-control form-control-alternative">
                    <option>Select Status</option>
                    <option>Active</option>
                    <option>Inactive</option>
                    <option>Out of Stock</option>
                  </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Update</button>
            </div>
          </div>
        </div>
      </div>


    </div>
    <?php include 'includes/script.php'; ?>
    <script type="text/javascript" src="js/view-products.js?ver=<?php echo $randStr; ?>"></script>
  </body>
</html>