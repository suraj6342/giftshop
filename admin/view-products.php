<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>
        <link rel="stylesheet" type="text/css" href="css/view-products.css?ver=<?php echo $randStr; ?>">
  </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content">
    <?php include 'includes/navbar.php'; ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card shadow">
              <div class="card-header border-0">
                <h3 class="mb-0 text-uppercase">Product List</h3>
              </div>
              <div class="table-responsive view-products-table">
                <table class="table align-items-center table-flush table-hover view-product-table">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Title</th>
                      <th scope="col">Category</th>
                      <th scope="col">SKU</th>
                      <th scope="col">Primary Color</th>
                      <th scope="col">Size</th>
                      <th scope="col">Price</th>
                      <th scope="col">MSRP</th>
                      <th scope="col">Sale Price</th>
                      <th scope="col">Available Quantity</th>
                      <th scope="col">Status</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Lady E Junior Girls Pink and White Strip</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">Apparel/Girls/Polo Shirts</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">1 Varient</span>
                          <a href="javscript:void(0)" class="show-variant" data-product-id="1" onclick="show_hide_variants(this)">(show)</a>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                      <td>
                          <li class="dropdown">
                            <a class="pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <div class="media align-items-center">
                                <div class="media-body ml-2 d-none d-lg-block">
                                  <span class="mb-0 text-sm  font-weight-bold">Action</span>
                                </div>
                              </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                              <a href="product-details.php" class="dropdown-item">
                                <i class="ni ni-single-02"></i>
                                <span>Basic Details</span>
                              </a>
                              <a href="edit-product.php" class="dropdown-item">
                                <i class="ni ni-settings-gear-65"></i>
                                <span>Edit Basic Details</span>
                              </a>
                              <a href="add-variant.php" class="dropdown-item">
                                <i class="ni ni-calendar-grid-58"></i>
                                <span>Add Variant</span>
                              </a>
                              <a class="dropdown-item modal-open-btn" data-toggle="modal" data-target="#update-product-status"> 
                                <i class="ni ni-support-16"></i>
                                <span>Update Status</span>
                              </a>
                            </div>
                          </li>
                      </td>
                    </tr>
                    <tr class="variant-table-row" id="variant_1">
                      <th scope="row">
                          <span class="mb-0 text-sm"></span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm"></span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">SDGP0203</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Pink</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Small</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">$556</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">$466</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">$544</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">10</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                      <td>
                          <li class="dropdown">
                            <a class="pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <div class="media align-items-center">
                                <div class="media-body ml-2 d-none d-lg-block">
                                  <span class="mb-0 text-sm  font-weight-bold">Action</span>
                                </div>
                              </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                              <a href="product-variant-details.php" class="dropdown-item">
                                <i class="ni ni-single-02"></i>
                                <span>Variant Details</span>
                              </a>
                              <a href="product-variant-images.php" class="dropdown-item">
                                <i class="ni ni-settings-gear-65"></i>
                                <span>Varient Images</span>
                              </a>
                              <a href="edit-variant.php" class="dropdown-item">
                                <i class="ni ni-calendar-grid-58"></i>
                                <span>Edit Variant</span>
                              </a>
                              <a class="dropdown-item" data-toggle="modal" data-target="#update-variant-quantity">
                                <i class="ni ni-support-16"></i>
                                <span>Update Stock</span>
                              </a>
                              <a class="dropdown-item" data-toggle="modal" data-target="#update-variant-status">
                                <i class="ni ni-support-16"></i>
                                <span>Update Status</span>
                              </a>
                              <a href="./examples/profile.html" class="dropdown-item">
                                <i class="ni ni-support-16"></i>
                                <span>Add to Feature</span>
                              </a>
                            </div>
                          </li>
                      </td>
                    </tr>
                    
                  </tbody>
                </table>
              </div>
              <div class="card-footer py-4">
                <nav aria-label="...">
                  <ul class="pagination justify-content-end mb-0">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1">
                        <i class="fas fa-angle-left"></i>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item active">
                      <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#">
                        <i class="fas fa-angle-right"></i>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>    
      
      <!-- Update Product Status modal -->
      <div class="modal fade" id="update-product-status" tabindex="-1" role="dialog" aria-labelledby="update-product-statusLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="update-product-statusLabel">Update Product Status</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="form-control-label" for="menu-item">Product Status *</label>
                  <select type="text" id="menu-item" class="form-control form-control-alternative">
                    <option>Select Status</option>
                    <option>Active</option>
                    <option>Inactive</option>
                  </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Update</button>
            </div>
          </div>
        </div>
      </div>

      <!-- Update varient quality modal -->
      <div class="modal fade" id="update-variant-quantity" tabindex="-1" role="dialog" aria-labelledby="update-variant-quantityLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="update-variant-quantityLabel">Update Variant Quantity</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="form-control-label" for="menu-item">Stock Quantity *</label>
                  <input type="text" class="form-control form-control-alternative" placeholder="Stock Quality">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Update</button>
            </div>
          </div>
        </div>
      </div>


      <!-- Update varient status modal -->
      <div class="modal fade" id="update-variant-status" tabindex="-1" role="dialog" aria-labelledby="update-variant-statusLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="update-variant-statusLabel">Update Variant Status</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="form-control-label" for="menu-item">Product Vaient Status *</label>
                  <select type="text" id="menu-item" class="form-control form-control-alternative">
                    <option>Select Status</option>
                    <option>Active</option>
                    <option>Inactive</option>
                    <option>Out of Stock</option>
                  </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Update</button>
            </div>
          </div>
        </div>
      </div>


    </div>
    <?php include 'includes/script.php'; ?>
    <script type="text/javascript" src="js/view-products.js?ver=<?php echo $randStr; ?>"></script>
  </body>
</html>