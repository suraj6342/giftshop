<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>


  </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content">
    <?php include 'includes/navbar.php'; ?>
      <div class="container-fluid">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Sitepath Setting</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form>
                <div class="">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Site Name</label>
                        <input class="form-control form-control-alternative">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">TagLine</label>
                        <input class="form-control form-control-alternative">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Email</label>
                        <input class="form-control form-control-alternative">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Phone</label>
                        <input class="form-control form-control-alternative">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Address</label>
                        <textarea rows="4" class="form-control form-control-alternative" id="product-desc" >Address</textarea>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Facebook Link</label>
                        <input class="form-control form-control-alternative">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Twitter Link</label>
                        <input class="form-control form-control-alternative">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Google Plus Link</label>
                        <input class="form-control form-control-alternative">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Instagram Link</label>
                        <input class="form-control form-control-alternative">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Pinterest Link</label>
                        <input class="form-control form-control-alternative" >
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <button class="btn btn-icon btn-3 btn-primary" type="button">
                          <span class="btn-inner--text">Submit</span>
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>    
    </div>
    <?php include 'includes/script.php'; ?>
  </body>
</html>