<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>


  </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content">
    <?php include 'includes/navbar.php'; ?>
      <div class="container-fluid">
        <div class="row user-filter-card-main">
          <div class="col">
            <div class="card shadow mb-12 col-sm-12 no-pad-lr">
              <div class="card-header form-inline user-filter-card">
                <div class="form-group col-sm-3 no-pad-l">
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-user"></i></span>
                    </div>
                    <input class="form-control" placeholder="Search By Name" type="text">
                  </div>
                </div>
                <div class="form-group col-sm-3 no-pad-l">
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-envelope"></i>  </span>
                    </div>
                    <input class="form-control" placeholder="Search by Email" type="text">
                  </div>
                </div>
                <div class="form-group col-sm-3 no-pad-l">
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-mobile-alt"></i></span>
                    </div>
                    <input class="form-control" placeholder="Search by Phone" type="text">
                  </div>
                </div>
                <div class="form-group col-sm-3 no-pad-l">
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-city"></i></span>
                    </div>
                    <input class="form-control" placeholder="Search by City" type="text">
                  </div>
                </div>
                <div class="form-group col-sm-12 no-pad-lr">
                  <button class="btn btn-icon btn-3 btn-primary" type="button">
                      <span class="btn-inner--icon"><i class="fas fa-filter"></i></span>
                      <span class="btn-inner--text">Filter</span>
                  </button>
                  <button class="btn btn-icon btn-3 btn-primary" type="button">
                      <span class="btn-inner--icon"><i class="fas fa-undo"></i></span>
                      <span class="btn-inner--text">Clear Filter</span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card shadow">
              <div class="card-header border-0">
                <h3 class="mb-0 text-uppercase">User List</h3>
              </div>
              <div class="table-responsive">
                <table class="table align-items-center table-flush table-hover user-list-table">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Address1</th>
                      <th scope="col">Address2</th>
                      <th scope="col">City</th>
                      <th scope="col">Country</th>
                      <th scope="col">Pin</th>
                      <th scope="col">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Mr. Abc Xyz</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">abcxyz@gmail.com</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">9874566121</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">House No:100</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Adabari Tiniali</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Guwahati</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">India</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">784568</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Mr. Abc Xyz</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">abcxyz@gmail.com</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">9874566121</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">House No:100</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Adabari Tiniali</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Guwahati</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">India</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">784568</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Mr. Abc Xyz</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">abcxyz@gmail.com</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">9874566121</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">House No:100</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Adabari Tiniali</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Guwahati</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">India</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">784568</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Mr. Abc Xyz</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">abcxyz@gmail.com</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">9874566121</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">House No:100</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Adabari Tiniali</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Guwahati</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">India</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">784568</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Mr. Abc Xyz</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">abcxyz@gmail.com</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">9874566121</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">House No:100</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Adabari Tiniali</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Guwahati</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">India</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">784568</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Mr. Abc Xyz</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">abcxyz@gmail.com</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">9874566121</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">House No:100</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Adabari Tiniali</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Guwahati</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">India</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">784568</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Mr. Abc Xyz</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">abcxyz@gmail.com</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">9874566121</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">House No:100</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Adabari Tiniali</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Guwahati</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">India</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">784568</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Mr. Abc Xyz</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">abcxyz@gmail.com</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">9874566121</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">House No:100</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Adabari Tiniali</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Guwahati</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">India</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">784568</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Mr. Abc Xyz</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">abcxyz@gmail.com</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">9874566121</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">House No:100</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Adabari Tiniali</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Guwahati</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">India</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">784568</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                    </tr>
                    
                  </tbody>
                </table>
              </div>
              <div class="card-footer py-4">
                <nav aria-label="...">
                  <ul class="pagination justify-content-end mb-0">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1">
                        <i class="fas fa-angle-left"></i>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item active">
                      <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#">
                        <i class="fas fa-angle-right"></i>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>    
    

    </div>
    <?php include 'includes/script.php'; ?>
  </body>
</html>