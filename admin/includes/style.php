<?php
    require_once "../config/setting.php";
?>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="css/argon.min.css?ver=<?php echo $randStr; ?>" rel="stylesheet">
<link href="css/style.css?ver=<?php echo $randStr; ?>" rel="stylesheet">
