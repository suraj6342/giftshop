<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>
        <link rel="stylesheet" type="text/css" href="css/view-order.css?ver=<?php echo $randStr; ?>">
  </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content">
    <?php include 'includes/navbar.php'; ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card shadow">
              <div class="card-header border-0">
                <h3 class="mb-0 text-uppercase">Order List</h3>
              </div>
              <div class="col-lg-12 order-select">
                <div class="form-group">
                  <select type="text" id="menu-item" class="form-control form-control-alternative">
                    <option>All</option>
                    <option>Pending</option>
                    <option>Shipped</option>
                    <option>Delivered</option>
                    <option>Cancelled</option>
                  </select>
                </div>
              </div>
              <div class="table-responsive view-products-table">
                <table class="table align-items-center table-flush table-hover view-product-table view-order-table">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Order Id</th>
                      <th scope="col">Order Date</th>
                      <th scope="col">Order Status</th>
                      <th scope="col">Payment Status</th>
                      <th scope="col">Transaction Id</th>
                      <th scope="col">Amount</th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">585c67c75a806401550305114</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">2019-02-16 13:48:34</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Delivered</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Approved</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">41199386648</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">$123</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm"><button class="btn btn-danger" data-toggle="modal" data-target="#order-detail-modal">Details</button></span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="card-footer py-4">
                <nav aria-label="...">
                  <ul class="pagination justify-content-end mb-0">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1">
                        <i class="fas fa-angle-left"></i>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item active">
                      <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#">
                        <i class="fas fa-angle-right"></i>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>    
      
      <!-- Order Detail modal -->
      <div class="modal fade" id="order-detail-modal" tabindex="-1" role="dialog" aria-labelledby="order-detail-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="order-detail-modalLabel">Order Details</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="card shadow">
                  <div class="table-responsive">
                    <table class="table align-items-center table-flush table-hover">
                      <tbody>
                        <tr>
                          <th scope="row">
                              <span class="mb-0 text-sm">Order Id:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">585c67c75a806401550305114</span>
                          </td>
                          <th scope="row">
                              <span class="mb-0 text-sm">Order Date:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">2019-02-16 13:48:34</span>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">
                              <span class="mb-0 text-sm">Order Status:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">Delivered</span>
                          </td>
                          <th scope="row">
                              <span class="mb-0 text-sm">Payment Status:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">Approved</span>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">
                              <span class="mb-0 text-sm">Transaction Id:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">41199386648</span>
                          </td>
                          <th scope="row">
                              <span class="mb-0 text-sm">Amount:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">$156</span>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">
                              <span class="mb-0 text-sm">Shipped Date:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">2019-02-16 13:56:23</span>
                          </td>
                          <th scope="row">
                              <span class="mb-0 text-sm">Courier Name:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">Fedex</span>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">
                              <span class="mb-0 text-sm">Tracking Id:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">1234</span>
                          </td>
                          <th scope="row">
                              <span class="mb-0 text-sm">Tracking URL:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">https://www.google.com/</span>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">
                              <span class="mb-0 text-sm">Deliver Date:</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">2019-02-16 13:56:43</span>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="card shadow billing-address-card mar-t-25">
                    <div class="card-header border-0">
                      <h4 class="mb-0 text-uppercase">Billing Address</h4>
                      <p>Test Name</p>
                      <p>House 4- first floor</p>
                      <p>venejuala, New York 1212355</p>
                    </div>
                </div>
                <div class="card shadow mar-t-25">
                  <div class="card shadow">
                      <div class="table-responsive">
                        <table class="table align-items-center table-flush table-hover">
                          <thead class="thead-light">
                            <tr>
                              <th scope="col">Product Title</th>
                              <th scope="col">Color</th>
                              <th scope="col">Size</th>
                              <th scope="col">SKU</th>
                              <th scope="col">Qty</th>
                              <th scope="col">Total</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">
                                  <span class="mb-0 text-sm">Polo Nect Shirt</span>
                              </th>
                              <td>
                                  <span class="mb-0 text-sm">Orange</span>
                              </td>
                              <td>
                                  <span class="mb-0 text-sm">M</span>
                              </td>
                              <td>
                                  <span class="mb-0 text-sm">ALKPVRGBC58538</span>
                              </td>
                              <td>
                                  <span class="mb-0 text-sm">255</span>
                              </td>
                              <td>
                                  <span class="mb-0 text-sm">$2055</span>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
          </div>
        </div>
      </div>

    </div>
    <?php include 'includes/script.php'; ?>
  </body>
</html>