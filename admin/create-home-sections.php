<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>


  </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content">
    <?php include 'includes/navbar.php'; ?>
      <div class="container-fluid">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Create Home Section</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form>
                <div class="">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="menu-item">Section *</label>
                        <select type="text" id="menu-item" class="form-control form-control-alternative">
                          <option>Select Section</option>
                          <option>Slider Section</option>
                          <option>Select Image</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Link *</label>
                        <input class="form-control form-control-alternative"  placeholder="Link">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="product-desc">Description *</label>
                        <input class="form-control form-control-alternative"  placeholder="Description">
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <button class="btn btn-icon btn-3 btn-primary" type="button">
                          <span class="btn-inner--text">Submit</span>
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>    
    </div>
    <?php include 'includes/script.php'; ?>
  </body>
</html>