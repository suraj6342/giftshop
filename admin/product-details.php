<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>
        <link rel="stylesheet" type="text/css" href="css/view-products.css?ver=<?php echo $randStr; ?>">
  </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content product-deatils-main">
    <?php include 'includes/navbar.php'; ?>
        <div class="container-fluid">
            <div class="row">
              <div class="col-sm-12">
                <div class="card shadow product-details-card">
                  <div class="card-header border-0">
                    <h3 class="mb-0 text-uppercase">Product Details</h3>
                  </div>
                  <div class="card-body">
                      <div class="row">
                        <div class="col-lg-6">
                            <div id="product-deatils-slider" class="carousel slide" data-ride="carousel">
                              <ol class="carousel-indicators">
                                <li data-target="#product-deatils-slider" data-slide-to="0" class="active"></li>
                                <li data-target="#product-deatils-slider" data-slide-to="1"></li>
                                <li data-target="#product-deatils-slider" data-slide-to="2"></li>
                                <li data-target="#product-deatils-slider" data-slide-to="3"></li>
                              </ol>
                              <div class="carousel-inner">
                                <div class="carousel-item active">
                                  <img src="images/product/dummy-product-1.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                  <img src="images/product/dummy-product-2.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                  <img src="images/product/dummy-product-1.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                  <img src="images/product/dummy-product-2.jpg" class="d-block w-100" alt="...">
                                </div>
                              </div>
                                <a class="carousel-control-prev" href="#product-deatils-slider" role="button" data-slide="prev">
                                  <span class="fas fa-chevron-left" aria-hidden="true"></span>
                                  <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#product-deatils-slider" role="button" data-slide="next">
                                  <span class="fas fa-chevron-right" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="table-responsive">
                              <table class="table">
                                <tbody>
                                  <tr>
                                    <th><h5>Title</h5></th>
                                    <th>:</th>
                                    <td><p>Sephlin - Lady E Junior Girls Pink and White Strip</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Menu Item</h5></th>
                                    <th>:</th>
                                    <td><p>Apparel</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Category</h5></th>
                                    <th>:</th>
                                    <td><p>Girls</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Sub Category</h5></th>
                                    <th>:</th>
                                    <td><p>Polo Shirts</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Sleeve</h5></th>
                                    <th>:</th>
                                    <td><p>Short Sleeves</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Fit</h5></th>
                                    <th>:</th>
                                    <td><p>Slim</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Fabric</h5></th>
                                    <th>:</th>
                                    <td><p>95% High Tech Polyester Construction + 5% Spandex</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Neck Type</h5></th>
                                    <th>:</th>
                                    <td><p>Collar</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Pattern</h5></th>
                                    <th>:</th>
                                    <td><p>Stripe, Sublimation</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Reversible</h5></th>
                                    <th>:</th>
                                    <td><p>No</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Fabric Care</h5></th>
                                    <th>:</th>
                                    <td><p>Machine Wash Cold, Gentle Cycle</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Status</h5></th>
                                    <th>:</th>
                                    <td><p>Active</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Create Date</h5></th>
                                    <th>:</th>
                                    <td><p>2019-03-19 14:03:40</p></td>
                                  </tr>
                                  <tr>
                                    <th><h5>Last Update</h5></th>
                                    <th>:</th>
                                    <td><p>2019-03-19 14:03:40</p></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        <div class="col-lg-12">
                          <h4 class="product-desc-head">Product Description:</h4>
                          <p>Sephlin Design Moisture Wicking Finish Polo Shirt. Very smooth finish and elegant look. Pink and White Double Stripe. 3 Button Placket. Light-weight. Breathable, Dri-Fit. Wicks the Sweat Away. For on and off the Golf Course. Fit for any occasion. Make a Statement!</p>
                        </div>
                        <div class="col-lg-12">
                          <h4 class="more-detail-head">More Details: </h4>
                          <p>Sephlin Design Moisture Wicking Finish Polo Shirt. Very smooth finish and elegant look. Pink and White Double Stripe. 3 Button Placket. Light-weight. Breathable, Dri-Fit. Wicks the Sweat Away. For on and off the Golf Course. Fit for any occasion. Make a Statement!</p>                          
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mar-t-30">
              <div class="col">
                <div class="card shadow ">
                  <div class="card-header border-0">
                    <h3 class="mb-0 text-uppercase">List of Variants</h3>
                  </div>
                  <div class="table-responsive">
                    <table class="table align-items-center table-flush table-hover view-product-table">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col">Primary Color</th>
                          <th scope="col">Secondary Color</th>
                          <th scope="col">Size</th>
                          <th scope="col">SKU</th>
                          <th scope="col">Stock</th>
                          <th scope="col">Price</th>
                          <th scope="col">MSRP</th>
                          <th scope="col">Sale Price</th>
                          <th scope="col">Last Update</th>
                          <th scope="col">Status</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">
                              <span class="mb-0 text-sm">Pink</span>
                          </th>
                          <td>
                              <span class="mb-0 text-sm">White</span>
                          </td>
                          <td>
                              <span class="mb-0 text-sm">Small</span>
                          </td>
                          <td>
                              <span class="mb-0 text-sm">SDGP0203</span>
                          </td>
                          <td>
                              <span class="mb-0 text-sm">5</span>
                          </td>
                          <td>
                              <span class="mb-0 text-sm">$636</span>
                          </td>
                          <td>
                              <span class="mb-0 text-sm">$454</span>
                          </td>
                          <td>
                              <span class="mb-0 text-sm">$655</span>
                          </td>
                          <td>
                              <span class="mb-0 text-sm">2019-03-19 14:03:40</span>
                          </td>
                          <td>
                              <span class="mb-0 text-sm">Active</span>
                          </td>
                          <td>
                              <a href="product-variant-details.php" class="anchor-btn">
                                <i class="ni ni-single-02"></i>
                                <span>View</span>
                              </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    <?php include 'includes/script.php'; ?>
  </body>
</html>
