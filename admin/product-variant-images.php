<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>
        <link rel="stylesheet" type="text/css" href="css/view-products.css?ver=<?php echo $randStr; ?>">
  </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content">
    <?php include 'includes/navbar.php'; ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card shadow">
              <div class="card-header border-0">
                <h3 class="mb-0 text-uppercase">Product Image</h3>
              </div>
              <div class="col-lg-12 no-pad-lr">
                <form method="POST" action="" enctype="multipart/form-data" name="image-upload-form" id="image-upload-form" novalidate="novalidate">
                  <div class="col-lg-12 no-pad-lr upload-main">
                      <div class="upload-form">
                        <input type="file" id="product_image" name="product_image">
                        <p>Drag or Click to upload your Images here.</p>
                      </div>
                  </div>
                  <input type="hidden" id="ip" name="ip">
                  <input type="hidden" id="user_id" name="user_id">
                  <input type="hidden" id="product_id" name="product_id">
                </form>
              </div>
              <div class="col-lg-12 uploaded-images">
                  <div class="col-lg-2 no-pad-l uploaded-images-tabs">
                    <img src="images/product/dummy-product-1.jpg" class="img-responsive">
                    <div class="col-lg-12 no-pad-lr uploaded-images-tabs-overlay">
                      <button class="btn btn-primary">Mark As Cover</button>
                      <button class="btn btn-primary">Delete</button>
                    </div>
                  </div>
                  <div class="col-lg-2 no-pad-l uploaded-images-tabs">
                    <img src="images/product/dummy-product-1.jpg" class="img-responsive">
                    <div class="col-lg-12 no-pad-lr uploaded-images-tabs-overlay">
                      <button class="btn btn-primary">Mark As Cover</button>
                      <button class="btn btn-primary">Delete</button>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>    
      
      <!-- Update Product Status modal -->
      <div class="modal fade" id="update-product-status" tabindex="-1" role="dialog" aria-labelledby="update-product-statusLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="update-product-statusLabel">Update Product Status</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label class="form-control-label" for="menu-item">Product Status *</label>
                  <select type="text" id="menu-item" class="form-control form-control-alternative">
                    <option>Select Status</option>
                    <option>Active</option>
                    <option>Inactive</option>
                  </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Update</button>
            </div>
          </div>
        </div>
      </div>

    </div>
    <?php include 'includes/script.php'; ?>
    <script type="text/javascript" src="js/view-products.js?ver=<?php echo $randStr; ?>"></script>
  </body>
</html>