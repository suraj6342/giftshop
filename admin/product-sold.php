<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>
        <link rel="stylesheet" type="text/css" href="css/view-products.css?ver=<?php echo $randStr; ?>">
  </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content">
    <?php include 'includes/navbar.php'; ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card shadow">
              <div class="card-header border-0">
                <h3 class="mb-0 text-uppercase">Sold Product Details</h3>
              </div>
              <div class="table-responsive view-products-table">
                <table class="table align-items-center table-flush table-hover view-product-table">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Product Title</th>
                      <th scope="col">Menu Item</th>
                      <th scope="col">Category</th>
                      <th scope="col">Sub Category</th>
                      <th scope="col">Primary Color</th>
                      <th scope="col">Size</th>
                      <th scope="col">Sale Price</th>
                      <th scope="col">Available Quantity</th>
                      <th scope="col">Status</th>
                      <th scope="col">Total Quantity Sold</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Lady E Junior Girls Pink and White Strip</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">Apparel</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Mens</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Polo Shirts</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Orange</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">M</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">2</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">99</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">1</span>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Lady E Junior Girls Pink and White Strip</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">Apparel</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Mens</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Polo Shirts</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Orange</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">M</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">2</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">99</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">1</span>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm">Lady E Junior Girls Pink and White Strip</span>
                      </th>
                      <td>
                          <span class="mb-0 text-sm">Apparel</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Mens</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Polo Shirts</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Orange</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">M</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">2</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">99</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">Active</span>
                      </td>
                      <td>
                          <span class="mb-0 text-sm">1</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="card-footer py-4">
                <nav aria-label="...">
                  <ul class="pagination justify-content-end mb-0">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1">
                        <i class="fas fa-angle-left"></i>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item active">
                      <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#">
                        <i class="fas fa-angle-right"></i>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>    
      
    </div>
    <?php include 'includes/script.php'; ?>
  </body>
</html>