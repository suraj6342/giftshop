<?php
    require_once "../config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ecommerce - Admin</title>
        <!-- Bootstrap -->
        <?php include 'includes/style.php'; ?>
        <link rel="stylesheet" type="text/css" href="css/pages.css?ver=<?php echo $randStr; ?>">
    </head>
  <body>
    <?php include 'includes/sidebar.php'; ?>
    <div class="main-content">
    <?php include 'includes/navbar.php'; ?>
      <div class="container-fluid">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Return Policy</h3>
                </div>
              </div>
            </div>
            <div class="card-body edit-section">
                <form method="post" action="" id="page-form" name="page-form" enctype="multipart/form-data">
                  <textarea id="page_content" name="page_content" placeholder="Your Content Here...!"></textarea>
                </form>
                <div class="col-12 no-pad-lr mar-t-25">
                  <button class="btn btn-primary">Update</button>
                </div>
            </div>
            <div class="card-body view-section">
                <div class="col-12 no-pad-lr">
                  <button class="btn btn-danger float-right" id="edit-page-btn">EDIT</button>
                </div>
                <div class="col-12 no-pad-lr float-left">
                  <h3 class="text-center">Wolcome To Product Page</h3>
                  <img src="../assets/images/label-watch.jpg" class="img-fluid">
                </div>
            </div>
          </div>
      </div>    
    </div>
    <?php include 'includes/script.php'; ?>

    <script type="text/javascript" src="js/pages.js"></script>
    <script type="text/javascript" src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  </body>
</html>