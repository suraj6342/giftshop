<?php
    require_once "config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Wishlist</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>
    <link rel="stylesheet" type="text/css" href="assets/css/contact.css?ver=<?php echo $randStr; ?>">

    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="col-xs-12 col-sm-12 no-pad-lr main-wrapper">
        <div class="col-xs-12 col-sm-12 contact-main">
          <h2>Get In Touch</h2>
          <div class="col-xs-12 col-sm-8 col-sm-offset-2 flex-middle">
              <div class="col-xs-12 col-sm-6">
                <div class="col-xs-12 col-sm-12 no-pad-lr contact-details-main">
                    <div class="col-xs-12 col-sm-12 contact-details-phone">
                        <div class="col-xs-3 col-sm-3 no-pad-lr">
                            <span class="contact-icon"><i class="fas fa-phone-volume"></i></span>
                        </div>
                        <div class="col-xs-9 col-sm-9 no-pad-lr">
                            <h4>Phone</h4>
                            <p>78945652435</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 contact-details-email">
                        <div class="col-xs-3 col-sm-3 no-pad-lr">
                            <span class="contact-icon"><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="col-xs-9 col-sm-9 no-pad-lr">
                          <h4>Email</h4>
                          <p>abcxyzdeum@icloud.com</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 contact-details-location">
                        <div class="col-xs-3 col-sm-3 no-pad-lr">
                          <span class="contact-icon"><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="col-xs-9 col-sm-9 no-pad-lr">
                          <h4>Our Location</h4>
                          <p>789, North Carolina, USA</p>
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="col-xs-12 col-sm-12">
                  <div class="col-xs-12 col-sm-12 no-pad-lr contact-form">
                    <input type="text" name="" placeholder="Name*" class="main-input">
                    <input type="text" name="" placeholder="Email*" class="main-input">
                    <textarea class="main-input" rows="4" placeholder="Type You Message...."></textarea>
                    <button class="main-btn" type="submit">SEND MESSAGE</button>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <?php include 'includes/footer.php'; ?>
    </div>

    <?php require_once 'includes/script.php'; ?>
  </body>
</html>