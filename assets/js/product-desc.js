function scrollup() {
  $('.carousel-indicators').scrollTo(0, 100);
}
function scrolldown() {
  $('.carousel-indicators').scrollTo(0, -100);
}

$(document).ready(function(){
	var input = $('.quantity_input'),
	    btnUp = $('button.up'),
	    btnDown = $('button.down');

	btnUp.on("click", function(){
	  var max = parseInt(input.attr("max")),
	      val = parseInt(input.val());
	  
	  if (val < max && val != max) {
	    val++;
	    input.val(val);
	  }
	});

	btnDown.on("click", function(){
	  var val = parseInt(input.val());
	  
	  if (val > 1) {
	    val--;
	    input.val(val);
	  }
	});

	input.on("focusout", function(){
	  var max = parseInt(input.attr("max")),
	      val = parseInt(input.val());
	  
	  if (val > max) {
	    input.val(max);
	  }
	});
});



