$('.slide-nav').on('click', function(e) {
    e.preventDefault();
    // get current slide
    var current = $('.flex--active').data('slide'),
      // get button data-slide
      next = $(this).data('slide');

    $('.slide-nav').removeClass('active');
    $(this).addClass('active');

    if (current === next) {
      return false;
    } else {
      $('.slider__warpper').find('.flex__container[data-slide=' + next + ']').addClass('flex--preStart');
      $('.flex--active').addClass('animate--end');
      setTimeout(function() {
        $('.flex--preStart').removeClass('animate--start flex--preStart').addClass('flex--active');
        $('.animate--end').addClass('animate--start').removeClass('animate--end flex--active');
      }, 800);
    }
});
$('.menu-wrapper').on('click', function() {
    $('.hamburger-menu').toggleClass('animate');
})


$(document).ready(function(){
      $('.slide-nav').click(function(){
          $('.slider__navi a.active').removeClass('active');
          $(this).addClass('active');
      });
      let currentLink = 1;
      let linkLengths = $('.slide-nav').length;
      setInterval(function(){
          $('.slide-nav:nth-child(' + currentLink++ +')').click();
          if(currentLink == linkLengths+1 ){
              currentLink = 1;
          }
      }, 3000)
});

$("#leftside-navigation .mobile-sub-menu > a").click(function(e) {
    $("#leftside-navigation ul ul").slideUp(), $(this).next().is(":visible") || $(this).next().slideDown(),
    e.stopPropagation()
})


$('.mobile-nav-menu').click(function(){
    $(".mobile-navbar").animate({left: "0%" });
});

$('.close-sidebar').click(function(){
    $(".mobile-navbar").animate({left: "-100%" });
});


