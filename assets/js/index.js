$(function() {
    $('#category-carousel').jcarousel();
    $('#category-carousel').jcarouselAutoscroll({
        interval: 250000
    });

});
$(document).ready(function(){
    $('#featured-slider').jcarousel();
    $('#featured-slider').jcarouselAutoscroll({
        interval: 250000
    });

    $('#newcollection').jcarousel();
    $('#newcollection').jcarouselAutoscroll({
        interval: 250000
    });
});