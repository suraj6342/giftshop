<?php
    require_once "config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Order History</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>
    <link rel="stylesheet" type="text/css" href="assets/css/overview-wishlist-cart-common.css?ver=<?php echo $randStr; ?>">
   <link rel="stylesheet" type="text/css" href="assets/css/order-history.css?ver=<?php echo $randStr; ?>">

    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="col-xs-12 col-sm-12 no-pad-lr main-wrapper">
        <div class="col-xs-12 col-sm-12 tabs-main">
            <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner">
              <div class="col-xs-4 col-sm-4 tabs-main-inner-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner-tab-inner">
                      <a href="overview.php">
                          <img src="assets/images/icon/overview.svg" class="img-responsive">
                          <h2>Overview</h2>
                          <p>A snapshot of your account information</p>
                      </a>
                  </div>
              </div>
              <div class="col-xs-4 col-sm-4 tabs-main-inner-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner-tab-inner">
                      <a href="order-history.php" class="active">
                          <img src="assets/images/icon/order-history.svg" class="img-responsive">
                          <h2>Order History</h2>
                          <p>See Current Orders and track shipments</p>
                      </a>
                  </div>
              </div>
              <div class="col-xs-4 col-sm-4 tabs-main-inner-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner-tab-inner">
                      <a href="wishlist.php">
                          <img src="assets/images/icon/wishlist.svg" class="img-responsive">
                          <h2>Wishlist</h2>
                          <p>Your Wishlist</p>
                      </a>
                  </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 order-history-tab">
              <div class="hr-seperater">
                  <hr>
              </div>
              <div class="col-xs-12 col-sm-12 no-pad-lr text-center order-history-tab-inner">
                  <select class="main-input order-history-select">
                      <option value="">All</option>
                      <option value="">Pending</option>
                      <option value="">Shipped</option>
                      <option value="">Delivered</option>
                      <option value="">Cancelled</option>
                  </select>
              </div>
              <div class="col-xs-12 col-sm-12 text-center no-pad-lr no-order-para">
                <p>No Order Found</p>
              </div>
              <div class="col-xs-12 col-sm-12 no-pad-lr order-history-table">
                  <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>#Order Id</th>
                          <th>Order Date</th>
                          <th>Order Status</th>
                          <th>Payment Status</th>
                          <th>Transaction Id</th>
                          <th>Amount</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>#564641</td>
                          <td>16-02-2019</td>
                          <td>Delivered</td>
                          <td>Paid</td>
                          <td>#123465</td>
                          <td>$456</td>
                          <td><a href="order-detail.php" class="table-btn">Details</a></td>
                        </tr>
                        <tr>
                          <td>#5641</td>
                          <td>16-02-2019</td>
                          <td>Delivered</td>
                          <td>Paid</td>
                          <td>#123465</td>
                          <td>$456</td>
                          <td><a href="order-detail.php" class="table-btn">Details</a></td>
                        </tr>
                        <tr>
                          <td>#545441</td>
                          <td>16-02-2019</td>
                          <td>Delivered</td>
                          <td>Paid</td>
                          <td>#123465</td>
                          <td>$456</td>
                          <td><a href="order-detail.php" class="table-btn">Details</a></td>
                        </tr>
                        <tr>
                          <td>#12341</td>
                          <td>16-02-2019</td>
                          <td>Delivered</td>
                          <td>Paid</td>
                          <td>#123465</td>
                          <td>$456</td>
                          <td><a href="order-detail.php" class="table-btn">Details</a></td>
                        </tr>
                        <tr>
                          <td>#45681</td>
                          <td>16-02-2019</td>
                          <td>Delivered</td>
                          <td>Paid</td>
                          <td>#123465</td>
                          <td>$456</td>
                          <td><a href="order-detail.php" class="table-btn">Details</a></td>
                        </tr>
                        <tr>
                          <td>#21351</td>
                          <td>16-02-2019</td>
                          <td>Delivered</td>
                          <td>Paid</td>
                          <td>#123465</td>
                          <td>$456</td>
                          <td><a href="order-detail.php" class="table-btn">Details</a></td>
                        </tr>
                        <tr>
                          <td>#56641</td>
                          <td>16-02-2019</td>
                          <td>Delivered</td>
                          <td>Paid</td>
                          <td>#123465</td>
                          <td>$456</td>
                          <td><a href="order-detail.php" class="table-btn">Details</a></td>
                        </tr>

                      </tbody>
                  </table>
              </div>
            </div>
        </div>
        <?php include 'includes/footer.php'; ?>
    </div>

    <?php require_once 'includes/script.php'; ?>
  </body>
</html>