<?php
    require_once "config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>View Products</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>
    <link rel="stylesheet" type="text/css" href="assets/css/products.css?ver=<?php echo $randStr; ?>">
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css?ver=<?php echo $randStr; ?>">

    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="col-xs-12 col-sm-12 no-pad-lr main-wrapper">
        <div class="col-xs-12 col-sm-12 products-main-tab">
          <div class="col-xs-12 col-sm-3 product-filter-tab">
            
            <div class="col-xs-12 col-sm-12 price-filter filter-tab">
              <h4>Choose Price</h4>
              <button class="filter-close hidden-sm hidden-md hidden-lg"><i class="fas fa-arrow-left"></i></button>
              <div class="col-xs-12 col-sm-12 no-pad-lr price-range-slider">
                  <div class="a"></div>
                  <span>$<span class="price-min-span">0</span></span> &nbsp<i class="fas fa-minus"></i> <span>$<span class="price-max-span">1000</span></span>

                  <input type="text" class="price-min hidden" id="min" value="0" disabled="">
                  <input type="text" class="price-max hidden" id="max" value="1000" disabled="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 size-filter filter-tab">
              <h4>Size</h4>
              <div class="col-xs-12 col-sm-12 no-pad-lr size-filter-inner">
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> L
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> M
                  </label>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 color-filter filter-tab">
              <h4>Choose Color</h4>
              <div class="col-xs-12 col-sm-12 no-pad-lr size-filter-inner">
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Black
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Blue
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Orange
                  </label>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 brand-filter filter-tab">
              <h4>Our Brand</h4>
              <div class="col-xs-12 col-sm-12 no-pad-lr size-filter-inner">
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Allen Solly
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> U.S POLO
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Levis
                  </label>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 brand-filter filter-tab">
              <h4>Stock</h4>
              <div class="col-xs-12 col-sm-12 no-pad-lr size-filter-inner">
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Exclude Out Of Stock
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-9 product-list-tab">
            <div class="col-xs-12 col-sm-12 product-list-tab-head">
              <button class="main-btn pull-left filter-btn hidden-sm hidden-md hidden-lg"><i class="fas fa-filter"></i> Filter</button>
              <button class="main-btn pull-left grid-btn"><i class="fas fa-th"></i> Grid</button>
              <button class="main-btn pull-left list-btn"><i class="fas fa-list"></i> List</button>
              <select class="main-input sorting-btn pull-right">
                  <option >Sort By</option>
                  <option>Price Low to High</option>
                  <option>Price High to Low</option>
              </select>
            </div>
            <div class="col-xs-12 col-sm-12 no-pad-lr product-list-tab-main">
              <div class="col-xs-12 col-sm-12 no-pad-lr product-list-tab-main-inner">
                  <div class="col-xs-12 col-sm-4">
                      <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                            <a href="product-description.php">
                              <img src="assets/images/products/product5.jpeg" class="img-responsive"> 
                            </a>
                          </div>
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                              <a href="product-description.php">
                                <h4>Red Top</h4>
                                <span class="list-price pull-left">Polo Shirts</span>
                                <span class="price pull-right">$566</span>
                              </a>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 list-product-tab-bottom">
                          <a href="product-description.php">
                              <h4>Red Top</h4>
                              <p class="description">Printed Men Round Neck Dark Blue T-Shirt</p>
                              <p class="price">$566</p>
                          </a>
                      </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                      <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                            <a href="product-description.php">
                              <img src="assets/images/products/product5.jpeg" class="img-responsive"> 
                            </a>
                          </div>
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                              <a href="product-description.php">
                                <h4>Red Top</h4>
                                <span class="list-price pull-left">Polo Shirts</span>
                                <span class="price pull-right">$566</span>
                              </a>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 list-product-tab-bottom">
                          <a href="product-description.php">
                              <h4>Red Top</h4>
                              <p class="description">Printed Men Round Neck Dark Blue T-Shirt</p>
                              <p class="price">$566</p>
                          </a>
                      </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                      <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                            <a href="product-description.php">
                              <img src="assets/images/products/product5.jpeg" class="img-responsive"> 
                            </a>
                          </div>
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                              <a href="product-description.php">
                                <h4>Red Top</h4>
                                <span class="list-price pull-left">Polo Shirts</span>
                                <span class="price pull-right">$566</span>
                              </a>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 list-product-tab-bottom">
                          <a href="product-description.php">
                              <h4>Red Top</h4>
                              <p class="description">Printed Men Round Neck Dark Blue T-Shirt</p>
                              <p class="price">$566</p>
                          </a>
                      </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                      <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                            <a href="product-description.php">
                              <img src="assets/images/products/product5.jpeg" class="img-responsive"> 
                            </a>
                          </div>
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                              <a href="product-description.php">
                                <h4>Red Top</h4>
                                <span class="list-price pull-left">Polo Shirts</span>
                                <span class="price pull-right">$566</span>
                              </a>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 list-product-tab-bottom">
                          <a href="product-description.php">
                              <h4>Red Top</h4>
                              <p class="description">Printed Men Round Neck Dark Blue T-Shirt</p>
                              <p class="price">$566</p>
                          </a>
                      </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                      <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                            <a href="product-description.php">
                              <img src="assets/images/products/product5.jpeg" class="img-responsive"> 
                            </a>
                          </div>
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                              <a href="product-description.php">
                                <h4>Red Top</h4>
                                <span class="list-price pull-left">Polo Shirts</span>
                                <span class="price pull-right">$566</span>
                              </a>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 list-product-tab-bottom">
                          <a href="product-description.php">
                              <h4>Red Top</h4>
                              <p class="description">Printed Men Round Neck Dark Blue T-Shirt</p>
                              <p class="price">$566</p>
                          </a>
                      </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                      <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                            <a href="product-description.php">
                              <img src="assets/images/products/product5.jpeg" class="img-responsive"> 
                            </a>
                          </div>
                          <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                              <a href="product-description.php">
                                <h4>Red Top</h4>
                                <span class="list-price pull-left">Polo Shirts</span>
                                <span class="price pull-right">$566</span>
                              </a>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 list-product-tab-bottom">
                          <a href="product-description.php">
                              <h4>Red Top</h4>
                              <p class="description">Printed Men Round Neck Dark Blue T-Shirt</p>
                              <p class="price">$566</p>
                          </a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <?php include 'includes/footer.php'; ?>
    </div>

    <?php require_once 'includes/script.php'; ?>
    <script type="text/javascript" src="assets/js/products.js?ver=<?php echo $randStr; ?>"></script>
  </body>
</html>