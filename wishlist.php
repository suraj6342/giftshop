<?php
    require_once "config/setting.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Wishlist</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>
    <link rel="stylesheet" type="text/css" href="assets/css/overview-wishlist-cart-common.css?ver=<?php echo $randStr; ?>">
    <link rel="stylesheet" type="text/css" href="assets/css/wishlist.css?ver=<?php echo $randStr; ?>">

    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="col-xs-12 col-sm-12 no-pad-lr main-wrapper">
        <div class="col-xs-12 col-sm-12 tabs-main">
            <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner">
              <div class="col-xs-4 col-sm-4 tabs-main-inner-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner-tab-inner">
                      <a href="overview.php">
                          <img src="assets/images/icon/overview.svg" class="img-responsive">
                          <h2>Overview</h2>
                          <p>A snapshot of your account information</p>
                      </a>
                  </div>
              </div>
              <div class="col-xs-4 col-sm-4 tabs-main-inner-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner-tab-inner">
                      <a href="order-history.php">
                          <img src="assets/images/icon/order-history.svg" class="img-responsive">
                          <h2>Order History</h2>
                          <p>See Current Orders and track shipments</p>
                      </a>
                  </div>
              </div>
              <div class="col-xs-4 col-sm-4 tabs-main-inner-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr tabs-main-inner-tab-inner">
                      <a href="wishlist.php" class="active">
                          <img src="assets/images/icon/wishlist.svg" class="img-responsive">
                          <h2>Wishlist</h2>
                          <p>Your Wishlist</p>
                      </a>
                  </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 no-pad-lr text-center wishlist-tab-main">
              <div class="hr-seperater">
                  <hr>
              </div>
              <p>Your Wishlist is empty</p>
            </div>
            <div class="col-xs-12 col-sm-12 no-pad-lr wishlist-main-tab">
                  <div class="col-xs-12 col-sm-12 no-pad-lr wishlist-main-tab-inner">
                      <div class="col-xs-12 col-sm-12 no-pad-lr wishlist-main-tab-inner-head">
                          <div class="col-xs-5 head-xs col-sm-4">
                            <h4>Product Details</h4>
                          </div>
                          <div class="col-xs-2 head-xs col-sm-3">
                            <h4>Price</h4>
                          </div>
                          <div class="col-xs-2 head-xs col-sm-2">
                            <h4>Delete</h4>
                          </div>
                          <div class="col-xs-3 head-xs col-sm-3 text-right">
                            <h4>Add To Cart</h4>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 no-pad-lr wishlist-main-tab-inner-item">
                          <div class="col-xs-5 col-sm-4 tab-list-xs wishlist-product-image-title">
                              <div class="col-xs-4 col-sm-4 no-pad-lr wishlist-product-image">
                                  <img src="assets/images/products/product3.jpeg" class="img-responsive">
                              </div>
                              <div class="col-xs-8 col-sm-8 wishlist-product-title">
                                  <h3>Levis Jeans Fit</h3>
                              </div>
                          </div>
                          <div class="col-xs-2 col-sm-3 tab-list-xs wishlist-product-price">
                              <p>$100.50</p>
                          </div>
                          <div class="col-xs-2 col-sm-2 tab-list-xs wishlist-delete-product">
                              <a href=""><img src="assets/images/icon/delete.svg" class="img-responsive"></a>
                          </div>
                          <div class="col-xs-3 col-sm-3 tab-list-xs wishlist-add-to-cart-btn text-right">
                              <button class="table-btn">Add To Cart</button>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 no-pad-lr wishlist-main-tab-inner-item">
                          <div class="col-xs-5 col-sm-4 tab-list-xs wishlist-product-image-title">
                              <div class="col-xs-4 col-sm-4 no-pad-lr wishlist-product-image">
                                  <img src="assets/images/products/product3.jpeg" class="img-responsive">
                              </div>
                              <div class="col-xs-8 col-sm-8 wishlist-product-title">
                                  <h3>Levis Jeans Fit</h3>
                              </div>
                          </div>
                          <div class="col-xs-2 col-sm-3 tab-list-xs wishlist-product-price">
                              <p>$100.50</p>
                          </div>
                          <div class="col-xs-2 col-sm-2 tab-list-xs wishlist-delete-product">
                              <a href=""><img src="assets/images/icon/delete.svg" class="img-responsive"></a>
                          </div>
                          <div class="col-xs-3 col-sm-3 tab-list-xs wishlist-add-to-cart-btn text-right">
                              <button class="table-btn">Add To Cart</button>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 no-pad-lr wishlist-main-tab-inner-item">
                          <div class="col-xs-5 col-sm-4 tab-list-xs wishlist-product-image-title">
                              <div class="col-xs-4 col-sm-4 no-pad-lr wishlist-product-image">
                                  <img src="assets/images/products/product3.jpeg" class="img-responsive">
                              </div>
                              <div class="col-xs-8 col-sm-8 wishlist-product-title">
                                  <h3>Levis Jeans Fit</h3>
                              </div>
                          </div>
                          <div class="col-xs-2 col-sm-3 tab-list-xs wishlist-product-price">
                              <p>$100.50</p>
                          </div>
                          <div class="col-xs-2 col-sm-2 tab-list-xs wishlist-delete-product">
                              <a href=""><img src="assets/images/icon/delete.svg" class="img-responsive"></a>
                          </div>
                          <div class="col-xs-3 col-sm-3 tab-list-xs wishlist-add-to-cart-btn text-right">
                              <button class="table-btn">Add To Cart</button>
                          </div>
                      </div>
                     
                  </div>
                </div>
        </div>
        <?php include 'includes/footer.php'; ?>
    </div>

    <?php require_once 'includes/script.php'; ?>
  </body>
</html>